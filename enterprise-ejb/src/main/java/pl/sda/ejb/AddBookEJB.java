/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.sda.ejb.model.Book;

/**
 *
 * @author martin
 */
@Stateless
@LocalBean 
public class AddBookEJB implements AddBookEJBRemote{
    
    @PersistenceContext(unitName = "persistence_unit")
    private EntityManager em;

    public AddBookEJB() {
    }

    @Override
    public List<Book> getBooks() {
        return em.createQuery("from Book").getResultList();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void addBook(Book book) {
        try{
            em.persist(book);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
