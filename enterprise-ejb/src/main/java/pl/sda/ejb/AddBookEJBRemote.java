/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import pl.sda.ejb.model.Book;

/**
 *
 * @author Adrian
 */
@Remote
@Local
public interface AddBookEJBRemote {    

    public List<Book> getBooks();

    public void addBook(Book b);
    
}
