/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.dto;

import pl.sda.ejb.model.Book;

public class BookDTO {

    private String title;
    private String isbn;
    private int releaseDate;

    public BookDTO(){}
    
    public BookDTO(String title, String isbn, int date){
        this.title = title;
        this.isbn = isbn;
        this.releaseDate = date;
    }
    
    public Book createBook(){
        Book b = new Book();
        b.setIsbn(this.isbn);
        b.setTitle(this.title);
        b.setReleaseDate(this.releaseDate);
        return b;
    }
    
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getReleaseDate() {
        return this.releaseDate;
    }

    public void setReleaseDate(int releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString() {
        return "pl.sda.beans.BookDTO[ id=" + this.isbn + " " + this.title +" ]";
    }
    
}
