/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import pl.sda.ejb.AddBookEJBRemote;
import pl.sda.ejb.dto.BookDTO;

/**
 *
 * @author Adrian
 */
@ManagedBean(name = "AddBook")
@RequestScoped
public class AddBookController {

    @EJB
    private AddBookEJBRemote ejb;
    private BookDTO book = new BookDTO();

    public String getText(){
        if (ejb!=null && ejb.getBooks()!=null && !ejb.getBooks().isEmpty()){
            System.out.println("COS TAM POWINIEN WYSWIETLIC");
            return ejb.getBooks().get(0).getTitle();
        }
        return this.book.getTitle();
    }
    
    public void setText(String text){
        this.book.setTitle(text);
        this.book.setIsbn("12345");
        this.book.setReleaseDate(2002);
        ejb.addBook(this.book.createBook());
    }
}
